use EcommerceChallenge

create table Customers(
	customer_id int PRIMARY KEY,
	firstName varchar(20),
	lastName varchar(20),
	email varchar(50),
	address varchar(max),
);

create table Products(
	product_id int PRIMARY KEY,
	[name] varchar(20),
	price Decimal(10,2),
	discription varchar(max),
	StockQuantity int
	);

create table Cart(
	cart_id int PRIMARY KEY,
	customer_id int,
	product_id int,
	quantity int,
	foreign Key(customer_id) references Customers(customer_id),
	foreign Key(product_id) references Products(product_id),
 );

 create table Orders(
	order_id int PRIMARY KEY,
	customer_id int ,
	order_date Date,
	total_price decimal(10,2),
	shipping_address varchar(max)
	Foreign key(customer_id) references Customers(customer_id)
 );

 create table order_items(
	order_item_id int Primary Key,
	order_id int,
	product_id int ,
	quantity int,
	itemAmount decimal(10,2),
	foreign Key(order_id) references Orders(order_id),
	foreign Key(product_id) references Products(product_id),

 );




 Insert into Products (product_id,name,discription,price,StockQuantity)
 values(1,'Laptop','High-performance laptop', 800.00, 10),
	(2, 'Smartphone', 'Latest smartphone', 600.00, 15),
	(3, 'Tablet', 'Portable tablet', 300.00, 20),
	(4, 'Headphones', 'Noise-canceling', 150.00, 30),
	(5, 'TV', '4K Smart TV', 900.00 ,5),
	(6, 'Coffee Maker', 'Automatic coffee maker', 50.00, 25),
	(7, 'Refrigerator' ,'Energy-efficient', 700.00, 10),
	(8, 'Microwave', 'Oven Countertop microwave', 80.00, 15),
	(9, 'Blender', 'High-speed blender', 70.00 ,20),
	(10, 'Vacuum Cleaner', 'Bagless vacuum cleaner' ,120.00, 10);


Insert into Customers(customer_id,firstName,lastName,email,address)
values(1, 'John','Doe', 'johndoe@example.com', '123 Main St, City'),
	(2, 'Jane', 'Smith', 'janesmith@example.com', '456 Elm St, Town'),
	(3 ,'Robert', 'Johnson' ,'robert@example.com' ,'789 Oak St, Village'),
	(4, 'Sarah','Brown', 'sarah@example.com', '101 Pine St, Suburb'),
	(5, 'David','Lee' ,'david@example.com' ,'234 Cedar St, District'),
	(6, 'Laura','Hall' ,'laura@example.com', '567 Birch St, County'),
	(7, 'Michael','Davis', 'michael@example.com', '890 Maple St, State'),
	(8, 'Emma','Wilson' ,'emma@example.com', '321 Redwood St, Country'),
	(9, 'William','Taylor' ,'william@example.com' ,'432 Spruce St, Province'),
	(10, 'Olivia','Adams', 'olivia@example.com', '765 Fir St, Territory');




Insert into Orders(order_id,customer_id,order_date,total_price)
values
(1, 1, '2023-01-05' ,1200.00),
(2, 2, '2023-02-10' ,900.00),
(3, 3, '2023-03-15' ,300.00),
(4, 4, '2023-04-20' ,150.00),
(5, 5, '2023-05-25' ,1800.00),
(6, 6, '2023-06-30' ,400.00),
(7, 7, '2023-07-05' ,700.00),
(8, 8, '2023-08-10' ,160.00),
(9, 9, '2023-09-15' ,140.00),
(10, 10, '2023-10-20' ,1400.00);

	


Insert into order_items(order_item_id,order_id,product_id,quantity,itemAmount)
values
(1, 1, 1, 2, 1600.00),
(2, 1, 3, 1, 300.00),
(3, 2, 2, 3, 1800.00),
(4, 3, 5, 2, 1800.00),
(5, 4, 4, 4, 600.00),
(6, 4, 6, 1, 50.00),
(7, 5, 1, 1, 800.00),
(8, 5, 2, 2, 1200.00),
(9, 6, 10, 2, 240.00),
(10, 6, 9, 3, 210.00);

Insert into Cart(cart_id,customer_id,product_id,quantity)
values
(1, 1, 1, 2),
(2, 1, 3, 1),
(3, 2, 2, 3),
(4, 3, 4, 4),
(5, 3, 5, 2),
(6, 4, 6, 1),
(7, 5, 1, 1),
(8, 6, 10, 2),
(9, 6, 9, 3),
(10, 7, 7, 2);




--1--
Update Products
set price=800
where [name]='Refrigerator'

select * from Products
where name='Refrigerator'


--2--
delete from cart 
where customer_id=4;


--3--
select * from Products
where price<100;

--4--
select * from Products
where StockQuantity>5

--5--
select * from Orders
where total_price>500 and total_price<1000


--6--
select * from Products
where name like 'r%'

--7--
select * from Cart 
where customer_id=5;

--8--
select c.firstName, c.lastName, o.order_date
from Customers c
join Orders o
on c.customer_id=o.customer_id
where year(o.order_date)=2023


--9-- Determine the Minimum Stock Quantity for Each Product Category.

--we do not contain product category therefore altering table
Alter table Products
add category varchar(20);

UPDATE products
SET category = 'Gadget'
WHERE product_id = 1;

UPDATE products
SET category = 'Gadget'
WHERE product_id = 2;

UPDATE products
SET category = 'Gadget'
WHERE product_id = 3;

UPDATE products
SET category = 'Gadget'
WHERE product_id = 4;

UPDATE products
SET category = 'Appliances'
WHERE product_id = 5;

SELECT category, MIN(StockQuantity) AS min_stock_quantity
FROM Products
GROUP BY category;



--10--
select c.customer_id,c.firstName,c.lastName,sum(oi.itemAmount)as amount_Spent
from Customers c
join orders o on c.customer_id=o.customer_id
join order_items oi on o.order_id=oi.order_id
group by c.customer_id,c.firstName,c.lastName



--11--
Select c.customer_id, c.firstName, c.lastName, AVG(o.total_price) AS average_order_amount
from Customers c
full join Orders o ON c.customer_id = o.customer_id
group by c.customer_id, c.firstName, c.lastName;


--12--
select c.customer_id,c.firstName,c.lastName,count(o.order_id) as NoOfOrders
from Customers c
left join Orders o
on c.customer_id=o.customer_id
group by c.customer_id,c.firstName,c.lastName


--13--
select c.customer_id, c.firstName, c.lastName, MAX(o.total_price) AS max_order_amount
from Customers c
left join Orders o ON c.customer_id = o.customer_id
group by c.customer_id, c.firstName, c.lastName;


--14--
select c.customer_id,c.firstName,c.lastName,SUM(o.total_price) as Order_Amount
from Customers c
join Orders o
on c.customer_id=o.customer_id
group by c.customer_id,c.firstName,c.lastName
having SUM(o.total_price)>1000


--15--
select p.product_id,p.[name]
from Products p 
where p.product_id not in (select product_id from Cart)


--16--
select customer_id,firstName,lastName
from Customers 
where customer_id not in(select customer_id from orders)


--17--
Select p.product_id, p.[name] AS product_name,(SUM(oi.itemAmount)/(Select SUM(itemAmount) from order_items))*100 as revenue_percentage
from Products p
Join order_items oi ON p.product_id = oi.product_id
Group by p.product_id, p.[name];


--18--
Select product_id, [name] as product_name, StockQuantity
from Products p
where StockQuantity < (Select AVG(StockQuantity) from Products) / 2;


--19--
SELECT c.customer_id, c.firstName, c.lastName
FROM Customers c
WHERE c.customer_id IN ( 
			SELECT o.customer_id
			from Orders o 
			WHERE o.total_price > (Select AVG(total_price) from Orders));

			